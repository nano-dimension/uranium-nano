from UM.Scene.SceneNodeDecorator import SceneNodeDecorator
from UM.Scene.Selection import Selection
from typing import TYPE_CHECKING, Optional

if TYPE_CHECKING:
    from UM.Scene.SceneNode import SceneNode


class ExactCopyDecorator(SceneNodeDecorator):
    def __init__(self, source_node: "SceneNode") -> None:
        super().__init__()
        # Used to keep track of previous parent when an empty group removes itself from the scene.
        # We keep this option so that it's possible to undo it.
        self.source_node = None  # type: Optional[SceneNode]
        self.setSourceNode(source_node)  # type: Optional[SceneNode]

    def setNode(self, node: "SceneNode") -> None:
        super().setNode(node)

    def setSourceNode(self, source_node: "SceneNode"):
        self.source_node = source_node  # type: Optional[SceneNode]

    def getSourceNode(self):
        return self.source_node  # type: Optional[SceneNode]

    def isCopy(self) -> bool:
        return True

    def __deepcopy__(self, memo):
        return ExactCopyDecorator(self.source_node)

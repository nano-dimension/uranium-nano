# Copyright (c) 2019 Ultimaker B.V.
# Uranium is released under the terms of the LGPLv3 or higher.

import os
import re
from enum import Enum
from typing import List

from UM.PluginObject import PluginObject


class FileReader(PluginObject):
    class PreReadResult(Enum):
        """Used as the return value of FileReader.preRead."""

        # The user has accepted the configuration dialog or there is no configuration dialog.
        # The plugin should load the data.
        accepted = 1
        # The user has cancelled the dialog so don't load the data.
        cancelled = 2
        # preRead has failed and no further processing should happen.
        failed = 3

    def __init__(self, application=None) -> None:
        super().__init__()

        self._application = application
        if  self._application is not None:
            application.getOnExitCallbackManager().addCallback(self.ExitApp)

        self._supported_extensions = []  # type: List[str]

    def acceptsFile(self, file_name):
        """Returns true if file_name can be processed by this plugin.
            This supports the * wildcard by using a regexp parser
        :return: boolean indication if this plugin accepts the file specified.
        """

        ext = os.path.splitext(file_name.lower())[-1]
        for extension in self._supported_extensions:

            # replace wildcard in filename with equivalent regexp
            extension = extension.replace('*', '.*')
            if re.match(re.compile(extension), ext):
                return True

        return False

    def preRead(self, file_name, *args, **kwargs):
        """Executed before reading the file. This is used, for example, to display an import
        configuration dialog. If a plugin displays such a dialog,
        this function should block until it has been closed.

        :return: indicating if the user accepted or canceled the dialog.
        """

        return FileReader.PreReadResult.accepted

    def read(self, file_name):
        """Read mesh data from file and returns a node that contains the data

        :return: data read.
        """

        raise NotImplementedError("Reader plugin was not correctly implemented, no read was specified")

    def ExitApp(self):

        # wait for it to finish (note: the waiting happens where the lock is, i.e. on the node side)
        self.waitJobDone()

        # Report we're done
        if self._application is not None:
            self._application.getOnExitCallbackManager().onCurrentCallbackFinished()

    def waitJobDone(self):
        pass  # please implement for the specific case or don't use if do not need to
